﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public enum CLASSES {
    BARBARIAN,
    BARD,
    CLERIC,
    DRUID,
    FIGHTER,
    MONK,
    PALADIN,
    RANGER,
    ROGUE,
    SORCERER,
    WARLOCK,
    WIZARD
}

public enum ALIGNMENTS {
    NEUTRAL,
    NEUTRAL_GOOD,
    NEUTRAL_EVIL,
    LAWFUL,
    LAWFUL_GOOD,
    LAWFUL_EVIL,
    CHAOTIC,
    CHAOTIC_GOOD,
    CHAOTIC_EVIL
}

public enum SKILLS {
    ACROBATICS,
    ANIMAL_HANDLING,
    ARCANA,
    ATHLETICS,
    DECEPTION,
    HISTORY,
    INSIGHT,
    INTIMIDATION,
    INVESTIGATION,
    MEDICINE,
    NATURE,
    PERCEPTION,
    PERFORMANCE,
    PERSUASION,
    RELIGION,
    SLEIGHT_OF_HAND,
    STEALTH,
    SURVIVAL
}

public enum STATS {
    DEX,
    CON,
    CHA,
    WIS,
    INT,
    STR
}

public class CardManager : MonoBehaviour {
    [System.Serializable]
    public struct CLASS_IMAGES {
        public Sprite BARBARIAN;
        public Sprite BARD;
        public Sprite CLERIC;
        public Sprite DRUID;
        public Sprite FIGHTER;
        public Sprite MONK;
        public Sprite PALADIN;
        public Sprite RANGER;
        public Sprite ROGUE;
        public Sprite SORCERER;
        public Sprite WARLOCK;
        public Sprite WIZARD;

        public Sprite GetClassImage(CLASSES c) {
            switch (c) {
                case CLASSES.BARBARIAN:
                    return BARBARIAN;
                case CLASSES.BARD:
                    return BARD;
                case CLASSES.CLERIC:
                    return CLERIC;
                case CLASSES.DRUID:
                    return DRUID;
                case CLASSES.FIGHTER:
                    return FIGHTER;
                case CLASSES.MONK:
                    return MONK;
                case CLASSES.PALADIN:
                    return PALADIN;
                case CLASSES.RANGER:
                    return RANGER;
                case CLASSES.ROGUE:
                    return ROGUE;
                case CLASSES.SORCERER:
                    return SORCERER;
                case CLASSES.WARLOCK:
                    return WARLOCK;
                case CLASSES.WIZARD:
                    return WIZARD;
                default:
                    return null;
            }
        }
    }

    [System.Serializable]
    public struct ALIGNMENT_IMAGES {
        public Sprite NEUTRAL_A;
        public Sprite GOOD;
        public Sprite EVIL;
        public Sprite NEUTRAL_B;
        public Sprite LAWFUL;
        public Sprite CHAOTIC;

        public Sprite GetGoodOrEvilImage(ALIGNMENTS a) {
            int i = ((int)a) % 3;

            switch (i) {
                case 0:
                    return NEUTRAL_A;
                case 1:
                    return GOOD;
                case 2:
                    return EVIL;
            }

            return null;
        }

        public Sprite GetLawOrChaosImage(ALIGNMENTS a) {
            int i = ((int)a) / 3;

            switch (i) {
                case 0:
                    return NEUTRAL_B;
                case 1:
                    return LAWFUL;
                case 2:
                    return CHAOTIC;
            }

            return null;
        }
    }

    [System.Serializable]
    public struct CARD_UI_ELEMENTS {
        public Text titleText;
        public Text raceText;
        public Text visionText;
        public Text passivePerceptionText;
        public Text levelText;
        public Text initiativeText;
        public Text armourText;
        public Text healthText;
        public Image[] classImage;
        public Image alignmentAImage;
        public Image alignmentBImage;
        public Image portrait;
        public Text strModText;
        public Text dexModText;
        public Text conModText;
        public Text intModText;
        public Text wisModText;
        public Text chaModText;
        public Text[] skillBonusTexts;
        public StatWindow statWindow;
        public Text attacksText;
        public Image[] statSaves;
    }

    [System.Serializable]
    public struct COLOR_ELEMENTS {
        public Image[] primaryColorImages;
        public Image[] secondaryColorImages;
        public Image[] tertiaryColorImages;
        public Image[] highlightImages;
        public Image[] trimColorImages;
        public Shadow[] trim2ColorImages;
        public Text[] textColorImages;
        public Renderer hexMap;
        public Renderer[] cardEdges;
    }

    [System.Serializable]
    public struct THEME_ELEMENTS {
        public Image[] cardBack;
        public Image highlightOverlay;

        public Image bannerA;
        public Image bannerB;
        public Image[] circleBack;
        public Image[] circleTrim;
        public Image[] titleBack;
        public Image[] titleTrim;
        public Image subTitleBack;
        public Image subTitleTrim;
        public Image shoulderBack;
        public Image shoulderTrim;
        public Image bodyBack;
        public Image bodyTrim;
    }

    [Header("Constants")]
    public ThemeDatabase themeDB;
    public CLASS_IMAGES classImages;
    public ALIGNMENT_IMAGES alignmentImages;
    public CARD_UI_ELEMENTS cardUIElements;
    public GameObject[] particleParents;
    public COLOR_ELEMENTS colorElements;
    public THEME_ELEMENTS themeElements;
    public HexRenderer hexMap;

    [Header("Character")]
    public CharacterData character;

    [Header("Card Colors")]
    public CardColors colors;
    
    /*
    void OnEnable() {
        Init();
    }
    */

    public void Init() {
        SetPortrait();
        SetTheme();

        cardUIElements.titleText.text = character.name;
        cardUIElements.raceText.text = character.race;
        cardUIElements.visionText.text = character.vision;
        cardUIElements.passivePerceptionText.text = character.passivePerception.ToString();
        cardUIElements.levelText.text = FormatMultisizeText("Level", character.level.ToString(), 16);
        cardUIElements.initiativeText.text = FormatMultisizeText("Initiative", (character.initiative >= 0 ? "+" : "") + character.initiative.ToString(), 16);

        foreach (Image i in cardUIElements.classImage) i.sprite = classImages.GetClassImage(character.playerClass);
        cardUIElements.alignmentAImage.sprite = alignmentImages.GetGoodOrEvilImage(character.alignment);
        cardUIElements.alignmentBImage.sprite = alignmentImages.GetLawOrChaosImage(character.alignment);
        hexMap.SetStats(character.stats.GetStatArray());
        cardUIElements.strModText.text = character.stats.GetStatModString(STATS.STR);
        cardUIElements.dexModText.text = character.stats.GetStatModString(STATS.DEX);
        cardUIElements.conModText.text = character.stats.GetStatModString(STATS.CON);
        cardUIElements.intModText.text = character.stats.GetStatModString(STATS.INT);
        cardUIElements.wisModText.text = character.stats.GetStatModString(STATS.WIS);
        cardUIElements.chaModText.text = character.stats.GetStatModString(STATS.CHA);

        for (int i = 0; i < cardUIElements.statSaves.Length; i++) {
            cardUIElements.statSaves[i].gameObject.SetActive(character.stats.GetSaveThrow((STATS)i));
        }

        cardUIElements.armourText.text = FormatMultisizeText("AC", character.armourClass.ToString());
        cardUIElements.healthText.text = FormatMultisizeText("HP", character.health.ToString());

        cardUIElements.attacksText.text = "";
        foreach (string s in character.attacks) {
            if (!string.IsNullOrEmpty(s)) {
                string[] lines = s.Split(new string[] { ". " }, System.StringSplitOptions.None);
                cardUIElements.attacksText.text += lines[0] + ".\n";
                for(int i = 1; i < lines.Length-1; i++) {
                    cardUIElements.attacksText.text += lines[i] + ". ";
                }
                cardUIElements.attacksText.text += lines[lines.Length-1] + "\n\n";
            }
        }
        if (!string.IsNullOrEmpty(cardUIElements.attacksText.text))
            cardUIElements.attacksText.text = cardUIElements.attacksText.text.Remove(cardUIElements.attacksText.text.Length - 2);

        foreach (Image i in colorElements.primaryColorImages) {
            i.color = colors.primary;
        }
        foreach (Image i in colorElements.secondaryColorImages) {
            i.color = colors.secondary;
        }
        foreach (Image i in colorElements.tertiaryColorImages) {
            i.color = colors.tertiary;
        }
        foreach (Image i in colorElements.highlightImages) {
            i.color = colors.highlight;
        }
        if(themeElements.highlightOverlay.sprite == null) {
            themeElements.highlightOverlay.color = Color.clear;
        }

        foreach (Image i in colorElements.trimColorImages) {
            i.color = colors.trim;
        }
        foreach (Shadow i in colorElements.trim2ColorImages) {
            i.effectColor = colors.secondaryTrim;
        }
        foreach (Text i in colorElements.textColorImages) {
            i.color = colors.textColor;
        }
        if(colorElements.hexMap != null)
            colorElements.hexMap.material.color = colors.highlight;
        foreach (Renderer r in colorElements.cardEdges)
            r.material.color = colors.trim;

        cardUIElements.statWindow.InitWindow(character.stats, CharacterData.GetProfBonus(character.level), character.skillProfs);

        for (int i = 0; i < particleParents.Length; i++) {
            if (particleParents[i] != null) {
                if ((int)character.flare == i) {
                    particleParents[i].SetActive(true);
                    ParticleSystem[] ps = particleParents[i].GetComponentsInChildren<ParticleSystem>();
                    foreach (ParticleSystem p in ps) {
                        p.Stop();
                        p.startColor = colors.highlight;
                        p.Play();
                    }
                } else {
                    particleParents[i].SetActive(false);
                }
            }
        }
    }

    void SetTheme() {
        var themeObj = themeDB.GetTheme(character.theme);


        foreach (Image i in themeElements.cardBack) {
            i.sprite = themeObj.cardBack;
        }
        themeElements.highlightOverlay.sprite = themeObj.highlightOverlay;

        themeElements.bannerA.sprite = themeObj.bannerA;
        themeElements.bannerB.sprite = themeObj.bannerB;

        foreach (Image i in themeElements.circleBack) {
            i.sprite = themeObj.circleBack;
        }
        foreach (Image i in themeElements.circleTrim) {
            i.sprite = themeObj.circleTrim;
        }
        foreach (Image i in themeElements.titleBack) {
            i.sprite = themeObj.titleBack;
        }
        foreach (Image i in themeElements.titleTrim) {
            i.sprite = themeObj.titleTrim;
        }
        themeElements.subTitleBack.sprite = themeObj.subTitleBack;
        themeElements.subTitleTrim.sprite = themeObj.subTitleTrim;
        themeElements.shoulderBack.sprite = themeObj.shoulderBack;
        themeElements.shoulderTrim.sprite = themeObj.shoulderTrim;
        themeElements.bodyBack.sprite = themeObj.bodyBack;
        themeElements.bodyTrim.sprite = themeObj.bodyTrim;

}

    void SetPortrait() {
        cardUIElements.portrait.rectTransform.anchoredPosition = Vector2.zero;
        cardUIElements.portrait.rectTransform.sizeDelta = new Vector2(
            Mathf.Min(cardUIElements.portrait.rectTransform.sizeDelta.x,
            cardUIElements.portrait.rectTransform.sizeDelta.y), 
            Mathf.Min(cardUIElements.portrait.rectTransform.sizeDelta.x,
            cardUIElements.portrait.rectTransform.sizeDelta.y));

        cardUIElements.portrait.sprite = character.portraitImage;

        if (character.offsetInfo.is_wide) {
            cardUIElements.portrait.rectTransform.anchoredPosition = new Vector2(cardUIElements.portrait.rectTransform.sizeDelta.x * character.offsetInfo.offset_percentage, cardUIElements.portrait.rectTransform.anchoredPosition.y);
            cardUIElements.portrait.rectTransform.sizeDelta = new Vector2(cardUIElements.portrait.rectTransform.sizeDelta.x * character.offsetInfo.size_increase_percentage, cardUIElements.portrait.rectTransform.sizeDelta.y);
        } else {
            cardUIElements.portrait.rectTransform.anchoredPosition = new Vector2(cardUIElements.portrait.rectTransform.anchoredPosition.x, cardUIElements.portrait.rectTransform.sizeDelta.y * character.offsetInfo.offset_percentage);
            cardUIElements.portrait.rectTransform.sizeDelta = new Vector2(cardUIElements.portrait.rectTransform.sizeDelta.x, cardUIElements.portrait.rectTransform.sizeDelta.y * character.offsetInfo.size_increase_percentage);
        }
    }

    public void SetNewData(CharacterData cd, CardColors cc) {
        character = cd;
        colors = cc;
        Init();
    }

    string FormatMultisizeText(string smallText, string largeText, int textSize = 18) {
        return "<size=" + textSize + ">" + smallText + "\n</size>" + largeText;
    }
}

[System.Serializable]
public class CharacterData {
    public string timestamp;
    public Sprite portraitImage;
    public IMAGE_OFFSET_INFO offsetInfo = new IMAGE_OFFSET_INFO();
    public string name = "ExampleName";
    public string race = "ExampleRace";
    public string vision = "Normal Vision";
    public int passivePerception = 10;
    public int level = 1;
    public int initiative = 0;
    public CLASSES playerClass;
    public ALIGNMENTS alignment;
    public StatBlock stats;
    public int health;
    public int armourClass;
    public int[] skillProfs = new int[18];
    public string[] attacks = new string[3];
    public THEMES theme = THEMES.basic;
    public FLARES flare = FLARES.none;

    public static int GetProfBonus(int level) {
        if (level < 5) {
            return 2;
        } else if (level < 9) {
            return 3;
        } else if (level < 13) {
            return 4;
        } else if (level < 17) {
            return 5;
        } else {
            return 6;
        }
    }

    public static int GetHitDiceByClass(CLASSES c) {
        switch (c) {
            case CLASSES.BARBARIAN:
                return 12;
            case CLASSES.BARD:
                return 8;
            case CLASSES.CLERIC:
                return 8;
            case CLASSES.DRUID:
                return 8;
            case CLASSES.FIGHTER:
                return 10;
            case CLASSES.MONK:
                return 8;
            case CLASSES.PALADIN:
                return 10;
            case CLASSES.RANGER:
                return 10;
            case CLASSES.ROGUE:
                return 8;
            case CLASSES.SORCERER:
                return 6;
            case CLASSES.WARLOCK:
                return 8;
            case CLASSES.WIZARD:
                return 6;
        }
        return 0;
    }
}

[System.Serializable]
public class CardColors {
    public Color primary;
    public Color secondary;
    public Color tertiary;
    public Color trim;
    public Color secondaryTrim;
    public Color highlight;
    public Color textColor;
}

[System.Serializable]
public class StatBlock {
    public int Strength = 10;
    public bool StrSave = false;

    public int Dexterity = 10;
    public bool DexSave = false;

    public int Constitution = 10;
    public bool ConSave = false;

    public int Intelligence = 10;
    public bool IntSave = false;

    public int Wisdom = 10;
    public bool WisSave = false;

    public int Charisma = 10;
    public bool ChaSave = false;


    public int[] GetStatArray() {
        int[] stats = new int[6];

        stats[(int)STATS.STR] = Strength;
        stats[(int)STATS.DEX] = Dexterity;
        stats[(int)STATS.CON] = Constitution;
        stats[(int)STATS.INT] = Intelligence;
        stats[(int)STATS.WIS] = Wisdom;
        stats[(int)STATS.CHA] = Charisma;

        return stats;
    }

    public string GetStatModString(STATS stat) {
        int statMod = GetStatMod(stat);
        if(statMod >= 0) {
            return "+" + statMod.ToString();
        } else {
            return statMod.ToString();
        }
    }

    public int GetStatMod(STATS stat) {
        switch (stat) {
            case STATS.STR:
                return CalculateStatMod(Strength);
            case STATS.DEX:
                return CalculateStatMod(Dexterity);
            case STATS.CON:
                return CalculateStatMod(Constitution);
            case STATS.INT:
                return CalculateStatMod(Intelligence);
            case STATS.WIS:
                return CalculateStatMod(Wisdom);
            case STATS.CHA:
                return CalculateStatMod(Charisma);
            default:
                return 0;
        }
    }

    public bool GetSaveThrow(STATS stat) {
        switch (stat) {
            case STATS.STR:
                return StrSave;
            case STATS.DEX:
                return DexSave;
            case STATS.CON:
                return ConSave;
            case STATS.INT:
                return IntSave;
            case STATS.WIS:
                return WisSave;
            case STATS.CHA:
                return ChaSave;
            default:
                return false;
        }
    }

    public int GetSaveThrow(STATS stat, int level) {
        if (GetSaveThrow(stat)) {
            return GetStatMod(stat) + CharacterData.GetProfBonus(level);
        } else {
            return GetStatMod(stat);
        }
    } 

    public int GetStatModBySkill(SKILLS skill) {
        return GetStatMod(GetStatBySkill(skill));
    }

    public static STATS GetStatBySkill(SKILLS skill) {
        switch (skill) {
            case SKILLS.ACROBATICS:
                return (STATS.DEX);
            case SKILLS.ANIMAL_HANDLING:
                return (STATS.WIS);
            case SKILLS.ARCANA:
                return (STATS.INT);
            case SKILLS.ATHLETICS:
                return (STATS.STR);
            case SKILLS.DECEPTION:
                return (STATS.CHA);
            case SKILLS.HISTORY:
                return (STATS.INT);
            case SKILLS.INSIGHT:
                return (STATS.WIS);
            case SKILLS.INTIMIDATION:
                return (STATS.CHA);
            case SKILLS.INVESTIGATION:
                return (STATS.INT);
            case SKILLS.MEDICINE:
                return (STATS.WIS);
            case SKILLS.NATURE:
                return (STATS.INT);
            case SKILLS.PERCEPTION:
                return (STATS.WIS);
            case SKILLS.PERFORMANCE:
                return (STATS.CHA);
            case SKILLS.PERSUASION:
                return (STATS.CHA);
            case SKILLS.RELIGION:
                return (STATS.INT);
            case SKILLS.SLEIGHT_OF_HAND:
                return (STATS.DEX);
            case SKILLS.STEALTH:
                return (STATS.DEX);
            case SKILLS.SURVIVAL:
                return (STATS.WIS);
            default:
                return 0;
        }
    }

    public static int CalculateStatMod(int i) {
        return (i < 10) ? ((i - 11) / 2):((i - 10) / 2);
    }
}
