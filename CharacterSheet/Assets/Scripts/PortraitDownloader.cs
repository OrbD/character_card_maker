﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class PortraitDownloader : MonoBehaviour {

    public static string FOLDER_PATH;
    public delegate void OnDownloadComplete(string localURL);
    public delegate void OnConfirmation(string localURL, Sprite portraitImage, IMAGE_OFFSET_INFO positioningInfo);

    public GameObject URLWindow;
    public GameObject DownloadWindow;
    public GameObject ConfirmationWindow;

    public InputField URLInput;
    public Text errorMessage;
    public Text downloadTitle;
    public Image progressBar;
    public Image portrait;
    public DraggableImage portraitDrag;

    private string localPath;
    private Vector2 imageSizeMods;
    private OnConfirmation storedCallback;
    private WWW www;

	// Use this for initialization
	void Awake () {
        FOLDER_PATH = Application.persistentDataPath + "/portraits";
        if (!Directory.Exists(FOLDER_PATH)) {
            Directory.CreateDirectory(FOLDER_PATH);
        }

        URLWindow.SetActive(false);
        DownloadWindow.SetActive(false);
        ConfirmationWindow.SetActive(false);

        
}
	
	// Update is called once per frame
	void Update () {
        if (DownloadWindow.activeInHierarchy && www != null) {
            progressBar.fillAmount = www.progress;
        } 
	}

    public void OpenURLWindow(OnConfirmation callback) {
        storedCallback = callback;

        OpenURLWindow();
    }

    public void OpenURLWindow(bool showError = false) {
        gameObject.SetActive(true);
        URLWindow.SetActive(true);
        DownloadWindow.SetActive(false);
        ConfirmationWindow.SetActive(false);

        errorMessage.gameObject.SetActive(showError);

        localPath = "";
        URLInput.text = "";
    }

    public void NextClick() {
        if (!string.IsNullOrEmpty(URLInput.text)) {
            URLWindow.SetActive(false);
            DownloadWindow.SetActive(true);
            ConfirmationWindow.SetActive(false);

            StartCoroutine(DownloadImage(URLInput.text, OnDLComplete));
            downloadTitle.text = "Downloading...";
            progressBar.fillAmount = 0;
            portrait.rectTransform.sizeDelta = new Vector2(Mathf.Min(portrait.rectTransform.sizeDelta.x, portrait.rectTransform.sizeDelta.y), 
                Mathf.Min(portrait.rectTransform.sizeDelta.x, portrait.rectTransform.sizeDelta.y));
            portrait.rectTransform.anchoredPosition = Vector2.zero;
        }
    }

    void OnDLComplete (string localURL) {
        downloadTitle.text = "Loading Sprite...";
        this.localPath = localURL;
    }

    void ContinueToConfirmation(Vector2 sizeMod) {
        URLWindow.SetActive(false);
        DownloadWindow.SetActive(false);
        ConfirmationWindow.SetActive(true);
        imageSizeMods = sizeMod;
        
    }

    public void ConfirmClick() {
        storedCallback(localPath, portrait.sprite, portraitDrag.GetData());
        gameObject.SetActive(false);
        URLWindow.SetActive(false);
        DownloadWindow.SetActive(false);
        ConfirmationWindow.SetActive(false);
        localPath = "";
        URLInput.text = "";
        downloadTitle.text = "Downloading...";
        progressBar.fillAmount = 0;
        storedCallback = null;
    }

    public void CancelClick() {
        gameObject.SetActive(false);
        URLWindow.SetActive(false);
        DownloadWindow.SetActive(false);
        ConfirmationWindow.SetActive(false);
        localPath = "";
        URLInput.text = "";
        downloadTitle.text = "Downloading...";
        progressBar.fillAmount = 0;
        storedCallback = null;
    }

    IEnumerator DownloadImage(string url, OnDownloadComplete callback) {
        www = new WWW(url);
        yield return www;
        if (!string.IsNullOrEmpty(www.error)) {
            OpenURLWindow(true);
        } else {

            string[] urlBits = url.Split('.');
            string fileExtension = urlBits[urlBits.Length - 1];
            string localURL = DataReader.BuildPath(FOLDER_PATH, System.DateTime.Now.ToString("yyyy_MM_dd HH_mm_ss") + "." + fileExtension);

            if (fileExtension != "png" && fileExtension != "jpg" && fileExtension != "jpeg") {
                OpenURLWindow(true);
            } else {
                File.WriteAllBytes(localURL, www.bytes);
                callback(@"file:///" + localURL);

                www = new WWW(@"file:///" + localURL);
                yield return www;
                Texture2D tex = new Texture2D(4, 4, TextureFormat.DXT1, false);
                www.LoadImageIntoTexture(tex);
                portrait.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
                float aspectRatio = (portrait.sprite.texture.width+0.0f)/ portrait.sprite.texture.height ;
                Vector2 sizeMods = new Vector2((aspectRatio >= 1 ? aspectRatio : 1), (aspectRatio < 1 ? 1 / aspectRatio : 1));
                portrait.rectTransform.sizeDelta = new Vector2(portrait.rectTransform.sizeDelta.x * sizeMods.x, portrait.rectTransform.sizeDelta.y * sizeMods.y);
                ContinueToConfirmation(sizeMods);
            }
        }
    }
}
