﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class ThemeDatabase : ScriptableObject {

    public static ThemeDatabase singleton;
    public List<Theme> themeList;
    public Dictionary<THEMES, Theme> themeDictionary;

    void Awake() {
        singleton = this;
        themeDictionary = new Dictionary<THEMES, Theme>();
    }

    public Theme GetTheme(THEMES t) {
        if(themeDictionary == null || themeDictionary.Count < themeList.Count) {
            themeDictionary = new Dictionary<THEMES, Theme>();
            foreach (Theme iterator in themeList) {
                themeDictionary.Add(iterator.theme, iterator);
            }
        }

        if (themeDictionary.ContainsKey(t)) {
            return themeDictionary[t];
        } else {
            return null;
        }
    }


#if UNITY_EDITOR
    [MenuItem("Card Creator/Create Theme")]
    public static void CreateNew() {
        if (singleton == null) {
            ThemeDatabase database = new ThemeDatabase();
            AssetDatabase.CreateAsset(database, "Assets/ThemeData/ThemeList.asset");
        }

        Theme temp = new Theme();
        AssetDatabase.CreateAsset(temp, "Assets/ThemeData/NewTheme.asset");
    }
#endif
}
