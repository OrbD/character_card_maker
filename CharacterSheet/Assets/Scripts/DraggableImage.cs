﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public struct IMAGE_OFFSET_INFO {
    public bool is_wide;
    public float size_increase_percentage;
    public float offset_percentage;
}

public class DraggableImage : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

    private Image myImage;
    private bool isWide = true;
    private float offset;
    private float possibleOffset;
    private bool clicked = false;

    void Start() {
        myImage = gameObject.GetComponent<Image>();
    }

    public void OnPointerDown(PointerEventData eventData) {
        isWide = myImage.rectTransform.sizeDelta.x >= myImage.rectTransform.sizeDelta.y;
        if (isWide) {
            possibleOffset =(myImage.rectTransform.sizeDelta.x - myImage.rectTransform.sizeDelta.y)/2.0f;
            offset = myImage.rectTransform.anchoredPosition.x;
        } else {
            possibleOffset = (myImage.rectTransform.sizeDelta.y - myImage.rectTransform.sizeDelta.x)/2.0f;
            offset = myImage.rectTransform.anchoredPosition.y;
        }
        clicked = true;
    }

    Vector3 prevMousePos;
    void Update() {
        if (clicked) {
#if UNITY_STANDALONE_WIN || UNITY_EDITOR
            if (Input.GetMouseButtonDown(0)) {
                prevMousePos = Input.mousePosition;
            }

            if (Input.GetMouseButton(0)) {
                Vector2 deltaPos = Input.mousePosition - prevMousePos;
                prevMousePos = Input.mousePosition;
#elif UNITY_ANDROID
            if(Input.touchCount > 0) {
                Vector2 deltaPos = Input.GetTouch(0).deltaPosition;
#endif
                if (isWide) {
                    offset = Mathf.Clamp(deltaPos.x + offset, -possibleOffset, possibleOffset);
                    myImage.rectTransform.anchoredPosition = new Vector2(offset, myImage.rectTransform.anchoredPosition.y);
                } else {
                    offset = Mathf.Clamp(deltaPos.y + offset, -possibleOffset, possibleOffset);
                    myImage.rectTransform.anchoredPosition = new Vector2(myImage.rectTransform.anchoredPosition.x, offset);
                }
            }
        }
    }

    public void OnPointerUp(PointerEventData eventData) {
        clicked = false;
    }

    public IMAGE_OFFSET_INFO GetData() {
        var reObj = new IMAGE_OFFSET_INFO();
        reObj.is_wide = myImage.rectTransform.sizeDelta.x >= myImage.rectTransform.sizeDelta.y;
        if (reObj.is_wide) {
            reObj.size_increase_percentage = myImage.rectTransform.sizeDelta.x / myImage.rectTransform.sizeDelta.y;
            offset = myImage.rectTransform.anchoredPosition.x;
            reObj.offset_percentage = offset / myImage.rectTransform.sizeDelta.y;
        } else {
            reObj.size_increase_percentage = myImage.rectTransform.sizeDelta.y / myImage.rectTransform.sizeDelta.x;
            offset = myImage.rectTransform.anchoredPosition.y;
            reObj.offset_percentage = offset / myImage.rectTransform.sizeDelta.x;
        }
        return reObj;
    }
}
