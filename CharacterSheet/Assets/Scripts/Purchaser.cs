﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Purchasing;

public class Purchaser : MonoBehaviour {

    [System.Serializable]
    public struct ERROR_MESSAGE_WINDOW {
        public GameObject windowParent;
        public UnityEngine.UI.Text message;
    }

    public ERROR_MESSAGE_WINDOW errorWindow;

    public Animator windowAnimator;

    public Button nextBtn;
    public Button prevBtn;
    public GameObject buttonBlocker;
    public GameObject ownsEverythingOverlay;

    public IAPButton[] iapBtns;
    private int currentIndex = 0;

    void Awake() {
        if (!ReceiptValidator.GetItem(catalog_items.all_items) && !CardStoreListener.IsInitialised && CardStoreListener.StoreInitialised == null) {
            CardStoreListener.StoreInitialised += OnStoreLoad;
        } else {
            OnStoreLoad(true);
        }
        errorWindow.windowParent.SetActive(false);
        UpdateButtons();
    }

    public void ChangeButton(int change) {
        currentIndex += change;
        while (currentIndex >= iapBtns.Length) {
            currentIndex -= iapBtns.Length;
        }
        while (currentIndex < 0) {
            currentIndex += iapBtns.Length;
        }
        UpdateButtons();
    }

    private void UpdateButtons() {
        if (!ReceiptValidator.GetItem(catalog_items.all_items)) {
            int totalPs = ReceiptValidator.GetTotalPurchases();
            if (totalPs > 0) {
                iapBtns[0].productId = "flair_pack.all_" + totalPs.ToString();
            } else {
                iapBtns[0].productId = "flair_pack.all";
            }
            iapBtns[0].gameObject.SetActive(false);
            iapBtns[0].gameObject.SetActive(true);

            if (totalPs < 5) {
                nextBtn.gameObject.SetActive(true);
                prevBtn.gameObject.SetActive(true);
                buttonBlocker.SetActive(ReceiptValidator.GetItem((catalog_items)(currentIndex == 0?-1:currentIndex)));
                ownsEverythingOverlay.SetActive(false);
                for (int i = 0; i < iapBtns.Length; i++) {
                    iapBtns[i].gameObject.SetActive(i == currentIndex);
                }
            } else {
                nextBtn.gameObject.SetActive(false);
                prevBtn.gameObject.SetActive(false);
                buttonBlocker.SetActive(false);
                ownsEverythingOverlay.SetActive(false);
                for (int i = 1; i < iapBtns.Length; i++) {
                    iapBtns[i].gameObject.SetActive(false);
                }
            }
        } else {
            nextBtn.gameObject.SetActive(false);
            prevBtn.gameObject.SetActive(false);
            buttonBlocker.SetActive(false);
            ownsEverythingOverlay.SetActive(true);
            for (int i = 0; i < iapBtns.Length; i++) {
                iapBtns[i].gameObject.SetActive(false);
            }
        }
    }

    private void OnStoreLoad(bool success) {
        if (success)
            windowAnimator.SetTrigger("ShowProducts");
        else
            CloseWindow();
    }

    public void PurchaseComplete(Product boughtProduct) {
        CloseWindow();
    }

    public void PurchaseFailed (Product boughtProduct, PurchaseFailureReason failReason) {
        errorWindow.windowParent.SetActive(true);
        errorWindow.message.text = failReason.ToString();
    }

    public void CloseWindow() {
        Destroy(this.gameObject);
    }
}
