﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wobbler : MonoBehaviour {

    Transform parentObject;
    public Canvas frontCard;
    public Canvas backCard;

    bool showingFront;
    Quaternion backFacing = (Quaternion.Euler(0, 180, 0));
    Quaternion backFacing10Left = (Quaternion.Euler(0, 170, 0));
    Quaternion backFacing10Right = (Quaternion.Euler(0, 190, 0));
    Quaternion frontFacing10Left = (Quaternion.Euler(0, 10, 0));
    Quaternion frontFacing10Right = (Quaternion.Euler(0, 350, 0));

    Vector2 clickPos;

    float turnPercentage = 0.0f;
    float tiltPercentage = 0.0f;
    // Update is called once per frame
    void Update () {
#if UNITY_STANDALONE_WIN || UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
            clickPos = Input.mousePosition;

        if (Input.GetMouseButton(0)) {
            Vector2 mousePosDelta = (Vector2)Input.mousePosition - clickPos;
            clickPos = Input.mousePosition;

#elif UNITY_ANDROID
        if (Input.touchCount > 0) {
            Vector2 mousePosDelta = Input.GetTouch(0).deltaPosition;

#endif
            turnPercentage = Mathf.Lerp(turnPercentage, -(mousePosDelta.x / Screen.width) * 360f, Time.deltaTime * 5);
            tiltPercentage = (1 - Mathf.Abs(transform.rotation.eulerAngles.x >= 180 ? transform.rotation.eulerAngles.x - 360 : transform.rotation.eulerAngles.x) / 30) * (mousePosDelta.y / Screen.height) * 30f;
        } else if (turnPercentage != 0){
            turnPercentage = Mathf.Lerp(turnPercentage, 0, Time.deltaTime * 5);
            tiltPercentage = Mathf.Lerp(tiltPercentage, 0, Time.deltaTime * 5);
            if(turnPercentage < 0.5f && turnPercentage > -0.5f) {
                turnPercentage = 0.0f;
            }
        }

        if (turnPercentage != 0) {  
            transform.Rotate(tiltPercentage, turnPercentage, 0, Space.Self);
        } else {
            tiltPercentage = 0.0f;
            if (transform.rotation.eulerAngles.y < 260.0f && transform.rotation.eulerAngles.y > 100.0f)
                showingFront = false;
            else if (transform.rotation.eulerAngles.y > 280.0f || transform.rotation.eulerAngles.y < 80.0f)
                showingFront = true;
            transform.rotation = Quaternion.Slerp(transform.rotation, showingFront?(Quaternion.identity): backFacing, Time.deltaTime * 2f);
        }
        if (transform.rotation.eulerAngles.y < 270.0f && transform.rotation.eulerAngles.y > 90.0f) {
            backCard.sortingOrder = 2;
        } else {
            backCard.overrideSorting = true;
            frontCard.overrideSorting = true;
            frontCard.sortingOrder = 0;
            backCard.sortingOrder = -2;
        }
        
    }
}
