﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatWindow : MonoBehaviour {

    #region Consts
    public struct STAT_CONST {
        public string statName;
        public string[] statDescs;

        public string GetDescription(int statMod) {
            return statDescs[Mathf.Min(statMod + 5, 10)];
        }
    }

    public static STAT_CONST[] STAT_CONSTS = new STAT_CONST[] {
            new STAT_CONST {
                statName = "Dexterity",
                statDescs = new string[] {
                    "Barely mobile, probably significantly paralyzed.",
                    "Incapable of moving without noticeable effort or pain.",
                    "Visible paralysis or physical difficulty.",
                    "Significant klutz or very slow to react.",
                    "Somewhat slow, occasionally trips over own feet.",
                    "Capable of usually catching a small tossed object.",
                    "Able to often hit large targets with ranged weapons.",
                    "Able to often hit small targets with ranged weapons. Can catch or dodge a medium-speed surprise projectile.",
                    "Light on feet, able to often hit small moving targets.",
                    "Graceful, able to flow from one action into another easily. Capable of dodging a small number of thrown objects.",
                    "Moves like water, reacting to all situations with almost no effort. Capable of dodging a large number of thrown objects."
                }
            },
            new STAT_CONST {
                statName = "Constitution",
                statDescs = new string[] {
                    "Minimal immune system, body reacts violently to anything foreign.",
                    "Frail, suffers frequent broken bones.",
                    "Bruises very easily, knocked out by a light punch.",
                    "Unusually prone to disease and infection.",
                    "Easily winded, incapable of a full day’s hard labor.",
                    "Occasionally contracts mild sicknesses.",
                    "Can take a few hits before being knocked unconscious.",
                    "Easily shrugs off most illnesses. Able to labor for twelve hours most days.",
                    "Able to stay awake for days on end.",
                    "Very difficult to wear down, almost never feels fatigue.",
                    "Tireless paragon of physical endurance. Almost never gets sick, even to the most virulent diseases."
                }
            },
            new STAT_CONST {
                statName = "Charisma",
                statDescs = new string[] {
                    "Barely conscious, probably acts very alien. May have a presence which repels other people.",
                    "Minimal independent thought, relies heavily on others to think instead.",
                    "Has trouble thinking of others as people and how to interact with them.",
                    "Terribly reticent, uninteresting, or rude.",
                    "Something of a bore, makes people mildly uncomfortable or simply clumsy in conversation.",
                    "Capable of polite conversation.",
                    "Mildly interesting. Knows what to say to the right people.",
                    "Often popular or infamous. Knows what to say to most people and is very confident in debate.",
                    "Quickly likeable, respected or feared by many people. May be very eloquent. Good at getting their will when talking to people.",
                    "Quickly likeable, respected or feared by almost everybody. Can entertain people easily or knows how to effectively convince them of their own beliefs and arguments.",
                    "Renowned for wit, personality, and/or looks. May be a natural born leader."
                }
            },
            new STAT_CONST {
                statName = "Wisdom",
                statDescs = new string[] {
                    "Seemingly incapable of thought, barely aware.",
                    "Rarely notices important or prominent items, people, or occurrences.",
                    "Seemingly incapable of forethought.",
                    "Often fails to exert common sense.",
                    "Forgets or opts not to consider options before taking action.",
                    "Makes reasoned decisions most of the time.",
                    "Able to tell when a person is upset.",
                    "Reads people and situations fairly well. Can get hunches about a situation that doesn’t feel right.",
                    "Often used as a source of wisdom or decider of actions.",
                    "Reads people and situations very well, almost unconsciously.",
                    "Nearly prescient, able to reason far beyond logic."
                }
            },
            new STAT_CONST {
                statName = "Intelligence",
                statDescs = new string[] {
                    "Animalistic, no longer capable of logic or reason. Behavior is reduced to simple reactions to immediate stimuli.",
                    "Rather animalistic. Acts on instinct but can still resort to simple planning and tactics.",
                    "Very limited speech and knowledge. Often resorts to charades to express thoughts.",
                    "Has trouble following trains of thought, forgets most unimportant things.",
                    "Misuses and mispronounces words. May be forgetful.",
                    "Knows what they need to know to get by.",
                    "Knows a bit more than is necessary, fairly logical.",
                    "Fairly intelligent, able to understand new tasks quickly. Able to do math or solve logic puzzles mentally with reasonable accuracy.",
                    "Very intelligent, may invent new processes or uses for knowledge.",
                    "Highly knowledgeable, probably the smartest person many people know.",
                    "Famous as a sage and genius. Able to make Holmesian leaps of logic."
                }
            },
            new STAT_CONST {
                statName = "Strength",
                statDescs = new string[] {
                    "Morbidly weak, has significant trouble lifting own limbs.",
                    "Needs help to stand, can be knocked over by strong breezes.",
                    "Visibly weak. Might be knocked off balance by swinging something dense.",
                    "Difficulty pushing an object of their weight.",
                    "Has trouble lifting heavy objects for a longer time.",
                    "Lifts heavy objects for a short time. Can perform simple physical labor for a few hours without break.",
                    "Carries heavy objects and throws small objects for medium distances. Can perform physical labor for half a day without break.",
                    "Visibly toned. Carries heavy objects with one arm for longer distances. Doesn't get too exhausted by physical labor.",
                    "Muscular. Can break objects like wood with bare hands and raw strength. Can perform heavy physical labor for several hours without break.",
                    "Heavily muscular. Able to out-wrestle a work animal or catch a falling person. Performs the work of multiple people in physical labor.",
                    "Pinnacle of brawn, able to out-lift several people in combined effort."
                }
            }
        };
    #endregion

    [HideInInspector] public StatBlock stats;

    public Animator animationController;
    public Text t_Title;
    public Text t_Score;
    public Text t_Modifier;
    public Text t_SaveBonus;
    public Text t_Description;
    public SkillColumn skillList;

    private int profBonus;

    public void InitWindow(StatBlock cStats, int _profBonus, int[] _sProfList) {
        stats = cStats;
        profBonus = _profBonus;
        for (SKILLS i = SKILLS.ACROBATICS; i <= SKILLS.SURVIVAL; i++) {
            skillList.SetSkillBonus(i, stats.GetStatModBySkill(i) + (int)((_sProfList[(int)i] / 2.0f) * profBonus));
        }
    }

    public void OpenWindow(STATS displayStat) {
        if (ValidateWindowOpen()) {
            ActuallyOpenWindow(displayStat);
        } else {
            StartCoroutine(WaitToOpenWindow(displayStat));
        }
    }

    IEnumerator WaitToOpenWindow(STATS newStat) {
        yield return new WaitWhile(ValidateWindowOpen);
        ActuallyOpenWindow(newStat);
    }

    bool ValidateWindowOpen() {
        return animationController.GetCurrentAnimatorStateInfo(0).IsName("StatWindowClosed");
    }

    private void ActuallyOpenWindow(STATS displayStat) {
        t_Title.text = STAT_CONSTS[(int)displayStat].statName;
        t_Score.text = stats.GetStatArray()[(int)displayStat].ToString();

        int statMod = stats.GetStatMod(displayStat);
        int saveMod = stats.GetStatMod(displayStat) + (stats.GetSaveThrow(displayStat) ? profBonus : 0);

        t_Modifier.text = statMod >= 0 ? "+" + statMod.ToString() : statMod.ToString();
        t_SaveBonus.text = saveMod >= 0 ? "+" + saveMod.ToString() : saveMod.ToString();

        t_Description.text = STAT_CONSTS[(int)displayStat].GetDescription(stats.GetStatMod(displayStat));

        for (SKILLS s = SKILLS.ACROBATICS; s <= SKILLS.SURVIVAL; s++) {
            skillList.skillList[(int)s].gameObject.SetActive(StatBlock.GetStatBySkill(s) == displayStat);
        }
        animationController.SetTrigger("OpenWindow");
        animationController.ResetTrigger("CloseWindow");
    }

    public void CloseWindow() {
        animationController.SetTrigger("CloseWindow");
        animationController.ResetTrigger("OpenWindow");
    }
}
