﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(VerticalLayoutGroup))]
public class SkillColumn : MonoBehaviour {

    [HideInInspector] public SkillEntry[] skillList;
    bool inited = false;
    VerticalLayoutGroup myGroup;
    public SkillEntry exampleEntry;
    private float previousTAlpha = 0;
    public float textAlphas = 1;

    void Init() {
        myGroup = GetComponent<VerticalLayoutGroup>();
        skillList = new SkillEntry[(int)SKILLS.SURVIVAL + 1];
        for (SKILLS s = SKILLS.ACROBATICS; s <= SKILLS.SURVIVAL; s++) {
            skillList[(int)s] = Instantiate<GameObject>(exampleEntry.gameObject, this.transform).GetComponent<SkillEntry>();
            skillList[(int)s].SetName(s);
        }
        exampleEntry.gameObject.SetActive(false);
        inited = true;
    }

    void Update() {
        if (inited && previousTAlpha != textAlphas) {
            foreach (SkillEntry entry in skillList) {
                entry.SetAlpha(textAlphas);
            }
            previousTAlpha = textAlphas;
        }
    }

    public void SetSkillBonus(SKILLS skill, int bonus) {
        if (!inited)
            Init();
        skillList[(int)skill].SetValue(bonus);
    }
}
