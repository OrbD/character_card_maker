﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackButton : MonoBehaviour {

	void Update() {
        if (Input.GetKeyDown(KeyCode.Escape))
            SceneParser.instance.LoadOpeningMenu();
    }
}
