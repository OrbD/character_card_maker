﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorColumn : MonoBehaviour {
    public UnityEngine.UI.Image[] buttons;

    public ColorPicker cp;

    public void Init(ColorPicker manager) {
        cp = manager;
        buttons = GetComponentsInChildren<UnityEngine.UI.Image>();
    }

    public void ColorClicked(int index) {
        if (cp == null)
            Debug.Log("ColorPicker");
        if (buttons == null || buttons.Length == 0)
            Debug.Log("Buttons empty");
        if (cp == null)
            Debug.Log("ColorPicker");
        cp.RecieveColor(buttons[index].color);
    }
}
