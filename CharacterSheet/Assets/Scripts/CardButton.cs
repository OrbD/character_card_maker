﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardButton : MonoBehaviour {

    public Sprite emptySprite;

    public Text nameText;
    public Text subTitleText;
    public Text dateStampText;
    public Text errorMessage;
    public Image primaryColor;
    public Image tertiaryColor;
    public Image trimColor;
    public Image highlightColor;
    public Image buttonBacking;
    public Shadow buttonTrim;

    public int buttonIndex { get; private set; }
    public CharacterData storedCharacterData { get; private set; }
    public CardColors storedCardColors { get; private set; }
    public string portraitPath;

    public bool isLoaded { get; private set; }

    void Start() {
        errorMessage.gameObject.SetActive(false);
        //OpeningMenu.ButtonClickedCallback += OtherClicked;
    }

	public void SetData (int index, CharacterData cd, CardColors cc, string image_path) {
        buttonIndex = index;
        storedCharacterData = cd;
        storedCardColors = cc;
        portraitPath = image_path;
        if (image_path != "null") {
            StartCoroutine(LoadImage(portraitPath));
        } else {
            storedCharacterData.portraitImage = emptySprite;
            isLoaded = true;
        }
        UpdateButtonUI();
    }

    public IEnumerator LoadImage(string image_path) {
        isLoaded = false;
        WWW www = new WWW(image_path);
        yield return www;

        if (!string.IsNullOrEmpty(www.error)) {
            ImageError();
        } else {
            Texture2D tex = new Texture2D(4, 4, TextureFormat.DXT1, false);
            www.LoadImageIntoTexture(tex);
            Sprite sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
            FinishInit(sprite);
        }
    }

    private void ImageError() {
        errorMessage.gameObject.SetActive(true);
        storedCharacterData.portraitImage = emptySprite;
        isLoaded = true;
    }

    private void FinishInit(Sprite sprite) {
        storedCharacterData.portraitImage = sprite;
        isLoaded = true;
    }

    // Update is called once per frame
    void UpdateButtonUI () {
        nameText.text = storedCharacterData.name;
        subTitleText.text = storedCharacterData.race + " - " + storedCharacterData.playerClass.ToString()[0] + storedCharacterData.playerClass.ToString().ToLower().Substring(1) + " " + storedCharacterData.level.ToString();
        dateStampText.text = "Created: " + storedCharacterData.timestamp;

        nameText.color = storedCardColors.textColor;
        subTitleText.color = storedCardColors.textColor;
        dateStampText.color = storedCardColors.textColor;

        primaryColor.color = storedCardColors.primary;
        tertiaryColor.color = storedCardColors.tertiary;
        trimColor.color = storedCardColors.trim;
        highlightColor.color = storedCardColors.highlight;

        buttonBacking.color = storedCardColors.secondary;

        buttonTrim.effectColor = storedCardColors.secondaryTrim;
	}

    public void Clicked() {
        SceneParser.instance.LoadCardViewer(this);
    }

    public void EditClicked() {
        SceneParser.instance.LoadCardMaker(this);
    }

    public void DeleteClicked() {
        OpeningMenu.DeleteClickedCallback(buttonIndex);
    }

    void OtherClicked(int indexClicked) {
        if(indexClicked != buttonIndex && storedCardColors != null) {
            buttonTrim.effectColor = storedCardColors.secondaryTrim;
        }
    }
}
