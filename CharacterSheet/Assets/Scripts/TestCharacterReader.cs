﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCharacterReader : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(1)) {
            DataReader dr = new DataReader();
            CardManager cm = GetComponent<CardManager>();
            cm.character = dr.GetCharacterDataObj();
            cm.colors = dr.GetCardColorsObj();
            StartCoroutine(LoadImage(dr.GetPortraitImagePath()));
        }
    }

    public IEnumerator LoadImage(string image_path) {
        WWW www = new WWW(image_path);
        yield return www;
        Texture2D tex = new Texture2D(4, 4, TextureFormat.DXT1, false);
        www.LoadImageIntoTexture(tex);
        Sprite sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
        FinishInit(sprite);
    }

    void FinishInit(Sprite s) {
        CardManager cm = GetComponent<CardManager>();
        cm.character.portraitImage = s;
        cm.Init();
    }
}
