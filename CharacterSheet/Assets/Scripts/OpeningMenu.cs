﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public struct CHARACTER_JSON_OBJ {
    public string time_stamp;
    public string name;
    public string race;
    public int level;
    public string p_class;
    public string alignment;
    public int str;
    public bool str_save;
    public int dex;
    public bool dex_save;
    public int con;
    public bool con_save;
    public int inte;
    public bool int_save;
    public int wis;
    public bool wis_save;
    public int cha;
    public bool cha_save;
    public int hp;
    public int ac;
    public int init_bonus;
    public string senses;
    public int p_perception;

    public int acro_prof;
    public int anim_prof;
    public int arca_prof;
    public int athl_prof;
    public int dece_prof;
    public int hist_prof;
    public int insi_prof;
    public int inti_prof;
    public int inve_prof;
    public int medi_prof;
    public int natu_prof;
    public int perc_prof;
    public int perf_prof;
    public int pers_prof;
    public int reli_prof;
    public int slei_prof;
    public int stea_prof;
    public int surv_prof;

    public string attack_one;
    public string attack_two;
    public string attack_three;

    public string theme;
    public string flair;

    public string color_primary;
    public string color_secondary;
    public string color_tertiary;
    public string color_trim;
    public string color_trim2;
    public string color_highlight;
    public string color_text;

    public string portrait_path;
    public bool portrait_is_wide;
    public float portrait_offset;
    public float portrait_sizemod;

    public CharacterData GetCharacterData() {
        CharacterData re = new CharacterData();
        re.timestamp = time_stamp;
        re.name = name;
        re.race = race;
        re.level = level;
        re.health = hp;
        re.armourClass = ac;
        re.stats = new StatBlock();
        re.stats.Strength = str;
        re.stats.StrSave = str_save;
        re.stats.Dexterity = dex;
        re.stats.DexSave = dex_save;
        re.stats.Constitution = con;
        re.stats.ConSave = con_save;
        re.stats.Intelligence = inte;
        re.stats.IntSave = int_save;
        re.stats.Wisdom = wis;
        re.stats.WisSave = wis_save;
        re.stats.Charisma = cha;
        re.stats.ChaSave = cha_save;
        re.passivePerception = p_perception;
        re.initiative = init_bonus;
        re.vision = senses;

        for(CLASSES i = CLASSES.BARBARIAN; i <= CLASSES.WIZARD; i++) {
            if (i.ToString() == p_class)
                re.playerClass = i;
        }
        for(ALIGNMENTS i = ALIGNMENTS.NEUTRAL; i <= ALIGNMENTS.CHAOTIC_EVIL; i++) {
            if (i.ToString() == alignment)
                re.alignment = i;
        }
        for(SKILLS s = SKILLS.ACROBATICS; s <= SKILLS.SURVIVAL; s++) {
            re.skillProfs[(int)s] = GetSkillProf(s);
        }
        re.attacks[0] = attack_one;
        re.attacks[1] = attack_two;
        re.attacks[2] = attack_three;

        re.theme = ReceiptValidator.DecryptTheme(theme);
        re.flare = ReceiptValidator.DecryptEffect(flair);

        re.offsetInfo = new IMAGE_OFFSET_INFO();
        re.offsetInfo.is_wide = portrait_is_wide;
        re.offsetInfo.offset_percentage = portrait_offset;
        re.offsetInfo.size_increase_percentage = Mathf.Max(portrait_sizemod, 1);

        return re;
    }

    public CardColors GetCardColors() {
        CardColors re = new CardColors();
        ColorUtility.TryParseHtmlString(color_primary, out re.primary);
        ColorUtility.TryParseHtmlString(color_secondary, out re.secondary);
        ColorUtility.TryParseHtmlString(color_tertiary, out re.tertiary);
        ColorUtility.TryParseHtmlString(color_trim, out re.trim);
        ColorUtility.TryParseHtmlString(color_trim2, out re.secondaryTrim);
        ColorUtility.TryParseHtmlString(color_highlight, out re.highlight);
        ColorUtility.TryParseHtmlString(color_text, out re.textColor);
        return re;
    }

    public int GetSkillProf(SKILLS s) {
        switch (s) {
            case SKILLS.ACROBATICS:
                return acro_prof;
            case SKILLS.ANIMAL_HANDLING:
                return anim_prof;
            case SKILLS.ARCANA:
                return arca_prof;
            case SKILLS.ATHLETICS:
                return athl_prof;
            case SKILLS.DECEPTION:
                return dece_prof;
            case SKILLS.HISTORY:
                return hist_prof;
            case SKILLS.INSIGHT:
                return insi_prof;
            case SKILLS.INTIMIDATION:
                return inti_prof;
            case SKILLS.INVESTIGATION:
                return inve_prof;
            case SKILLS.MEDICINE:
                return medi_prof;
            case SKILLS.NATURE:
                return natu_prof;
            case SKILLS.PERCEPTION:
                return perc_prof;
            case SKILLS.PERFORMANCE:
                return perf_prof;
            case SKILLS.PERSUASION:
                return pers_prof;
            case SKILLS.RELIGION:
                return reli_prof;
            case SKILLS.SLEIGHT_OF_HAND:
                return slei_prof;
            case SKILLS.STEALTH:
                return stea_prof;
            case SKILLS.SURVIVAL:
                return surv_prof;
            default:
                return 0;
        }
    }
}

public class OpeningMenu : MonoBehaviour {

    public static System.Action<int> DeleteClickedCallback;

    public DeleteWindow deleteWindow;
	public CardButton ExampleButton;
    public RectTransform ContentPane;
    public List<CardButton> CardButtons;

    void Start() {
        DeleteClickedCallback = null;
        CardButtons = new List<CardButton>();

        ExampleButton.gameObject.SetActive(true);

        DataReader dr = new DataReader();
        for(int i = 0; i < dr.GetListLength(); i++) {
            CardButton temp = Instantiate<GameObject>(ExampleButton.gameObject, ContentPane).GetComponent<CardButton>();
            temp.SetData(i, dr.GetCharacterDataObj(i), dr.GetCardColorsObj(i), dr.GetPortraitImagePath(i));
            CardButtons.Add(temp);
        }
        ContentPane.sizeDelta = new Vector2(ContentPane.sizeDelta.x, 10 + 210 * dr.GetListLength());

        ExampleButton.gameObject.SetActive(false);

        DeleteClickedCallback += ButtonClicked;
    }

    int activeIndex = -1;

    void ButtonClicked(int buttonIndex) {
        deleteWindow.OpenWindow(CardButtons[buttonIndex]);
    }

    public void LoadClicked() {
        if(activeIndex != -1 && CardButtons.Count > 0 && CardButtons[activeIndex].isLoaded) {
            SceneParser.instance.LoadCardViewer(CardButtons[activeIndex]);
        }
    }

    public void EditClicked() {
        if (activeIndex != -1 && CardButtons.Count > 0 && CardButtons[activeIndex].isLoaded) {
            SceneParser.instance.LoadCardMaker(CardButtons[activeIndex]);
        }
    }

    public void NewClicked() {
        SceneParser.instance.LoadCardMaker();
    }
}


public class DataReader {

    public static string FOLDER_PATH = Application.persistentDataPath + "/save_data";
    public static string FILE_NAME = "cards.json";

    private List<CHARACTER_JSON_OBJ> characters = new List<CHARACTER_JSON_OBJ>();

    public DataReader() {
        if (!Directory.Exists(FOLDER_PATH)) {
            Debug.Log(FOLDER_PATH);
            Directory.CreateDirectory(FOLDER_PATH);
            CreateDatabase();
        } else if (!File.Exists(BuildPath(FOLDER_PATH, FILE_NAME))){
            CreateDatabase();
        } else {
            FillCharacterList(GetJsonArray());
        }
    }

    private void CreateDatabase() {
        Debug.Log(BuildPath(FOLDER_PATH, FILE_NAME));

        StreamWriter sw = new StreamWriter(BuildPath(FOLDER_PATH, FILE_NAME));
        sw.Write("[]");
        sw.Close();
    }

    public static string[] GetJsonArray() {
        StreamReader sr = new StreamReader(BuildPath(FOLDER_PATH, FILE_NAME));
        string rawData = sr.ReadToEnd();
        sr.Close();

        int recordingDepth = 0;
        List<string> dataList = new List<string>();
        string tempData = "";
        for(int i = 0; i < rawData.Length; i++) {
            if(rawData[i] == '{') {
                recordingDepth++;
            }
            if(recordingDepth > 0) {
                tempData += rawData[i];
            }
            if(rawData[i] == '}') {
                recordingDepth--;
                if(recordingDepth == 0) {
                    dataList.Add(tempData);
                    tempData = "";
                }
            }
        }

        return dataList.ToArray();
    }

    private void FillCharacterList(string[] data) {
        foreach(string node in data) {
            characters.Add(JsonUtility.FromJson<CHARACTER_JSON_OBJ>(node));
        }
    }

    public int GetListLength() {
        return characters.Count;
    }

    public CharacterData GetCharacterDataObj(int index = 0) {
        return characters[index].GetCharacterData();
    }

    public CardColors GetCardColorsObj(int index = 0) {
        return characters[index].GetCardColors();
    }

    public string GetPortraitImagePath(int index = 0) {
        return characters[index].portrait_path;
    }

    public static string BuildPath(params string[] args) {
        string builtPath = "";
        foreach(string arg in args) {
            builtPath += arg + "/";
        }
        if(!string.IsNullOrEmpty(builtPath))
            builtPath = builtPath.Remove(builtPath.Length - 1);
        return builtPath;
    }
}
