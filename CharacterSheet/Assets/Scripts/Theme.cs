﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Theme : ScriptableObject, IComparable<Theme> {
    public THEMES theme;
    public Sprite exampleBack;
    public Sprite exampleFront;
    public Color exampleColor;

    public Sprite cardBack;
    public Sprite highlightOverlay;

    public Sprite bannerA;
    public Sprite bannerB;
    public Sprite circleBack;
    public Sprite circleTrim;
    public Sprite titleBack;
    public Sprite titleTrim;
    public Sprite subTitleBack;
    public Sprite subTitleTrim;
    public Sprite shoulderBack;
    public Sprite shoulderTrim;
    public Sprite bodyBack;
    public Sprite bodyTrim;

    public int CompareTo(Theme other) {
        return (int)(theme - other.theme);
    }
}
