﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillEntry : MonoBehaviour {
    [SerializeField] Text skillName;
    [SerializeField] Text skillValue;

    public void SetName(SKILLS s) {
        string processedName = "";
        string[] sList = s.ToString().Split('_');
        for(int i = 0; i < sList.Length; i++) {
            if(i == 0 || i+1 == sList.Length) {
                processedName += sList[i][0] + sList[i].ToLower().Substring(1) + " ";
            } else {
                processedName += sList[i].ToLower() + " ";
            }
        }
        processedName = processedName.Substring(0, processedName.Length - 1);
        skillName.text = processedName;
    }

    public void SetValue(int value) {
        skillValue.text = value >= 0 ? "+" + value.ToString() : value.ToString();
    }

    public void SetAlpha(float a) {
        skillName.color = skillValue.color = new Color(skillName.color.r, skillName.color.g, skillName.color.b, a);
    }
}
