﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(StatButton))]
public class MenuButtonEditor : UnityEditor.UI.ButtonEditor {
    public override void OnInspectorGUI() {
        StatButton targetMenuButton = (StatButton)target;

        targetMenuButton.thisStat = (STATS)EditorGUILayout.EnumPopup("Stat", targetMenuButton.thisStat);
        targetMenuButton.statWin = (StatWindow)EditorGUILayout.ObjectField("Image", targetMenuButton.statWin, typeof(StatWindow), true);

        EditorGUILayout.Space();
        // Show default inspector property editor
        base.OnInspectorGUI();
    }
}
#endif

//[RequireComponent(typeof (Button))]
public class StatButton : Button {

    [SerializeField]
    public StatWindow statWin;
    public STATS thisStat;
    bool thisPressed;

    public override void OnPointerDown(PointerEventData e) {
        base.OnPointerDown(e);
        thisPressed = IsPressed();
        if (thisPressed) {
            statWin.OpenWindow(thisStat);
        }
    }

    public override void OnPointerUp(PointerEventData e) {
        base.OnPointerUp(e);
        if (thisPressed) {
            statWin.CloseWindow();
        }
        thisPressed = IsPressed();
    }
}


