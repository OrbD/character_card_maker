﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoreButton : MonoBehaviour {

    public GameObject StoreCanvas;

    public void OpenStore() {
        Instantiate(StoreCanvas);
    }
}
