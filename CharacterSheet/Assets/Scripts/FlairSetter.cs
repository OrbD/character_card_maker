﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlairSetter : MonoBehaviour {

    public ThemeDatabase tdb;
    public ColorEntry highlight;

    private THEMES currentTheme = THEMES.basic;
    private THEMES validTheme = THEMES.basic;
    private FLARES currentFlair = FLARES.none;
    private FLARES validFlair = FLARES.none;
    private int activeEffectIndex = 0;

    public Image backing;
    public Image front;
    public Text themeReading;
    public Text effectReading;

    public StoreButton storeBtn;

    void Awake() {
        storeBtn.gameObject.SetActive(false);
        SetTheme(currentTheme);
        SetFlair(currentFlair);
    }

    [System.Serializable]
    public struct Flair_Particle_Pair {
        public FLARES flair;
        public GameObject parent;
        public ParticleSystem[] particles;
    }
    public Flair_Particle_Pair[] exampleFlairs;

    public void ChangeTheme(int change) {
        int temp = (int)currentTheme + change;
        if(temp < (int)THEMES.basic) {
            temp = (int)THEMES.arcane;
        } else if(temp > (int)THEMES.arcane) {
            temp = (int)THEMES.basic;
        }
        SetTheme((THEMES)temp);
    }

    public void ChangeFlair(int change) {
        int temp = (int)currentFlair + change;
        if (temp < (int)FLARES.none) {
            temp = (int)FLARES.stars;
        } else if (temp > (int)FLARES.stars) {
            temp = (int)FLARES.none;
        }
        SetFlair((FLARES)temp);
    }

    public void SetTheme (THEMES t) {
        currentTheme = t;
        themeReading.text = t.ToString().ToUpper()[0] + t.ToString().ToLower().Substring(1).Replace('_', ' ');

        Theme style = tdb.GetTheme(t);
        if(style != null) {
            backing.sprite = style.exampleBack;
            front.sprite = style.exampleFront;
            SetParticleColor(exampleFlairs[activeEffectIndex].particles, style.exampleColor);
        }

        if(ValidateTheme(t)) {
            storeBtn.gameObject.SetActive(false);
            validTheme = t;
        } else {
            storeBtn.gameObject.SetActive(true);
        }
    }

    public bool ValidateTheme(THEMES t) {
        return (tdb.GetTheme(t) != null && ReceiptValidator.GetItem((catalog_items)t));
    }

    public void SetFlair(FLARES f) {
        currentFlair = f;
        effectReading.text = f.ToString().ToUpper()[0] + f.ToString().ToLower().Substring(1).Replace('_', ' ');

        for(int i = 0; i < exampleFlairs.Length; i++) {
            if(exampleFlairs[i].flair == f) {
                activeEffectIndex = i;
                exampleFlairs[i].parent.SetActive(true);
                SetParticleColor(exampleFlairs[i].particles, (tdb.GetTheme(currentTheme) == null) ? highlight.storedColor : tdb.GetTheme(currentTheme).exampleColor);
            } else {
                exampleFlairs[i].parent.SetActive(false);
            }
        }

        if (ValidateFlair(f)) {
            validFlair = f;
            storeBtn.gameObject.SetActive(false);
        } else {
            storeBtn.gameObject.SetActive(true);
        }
    }

    public bool ValidateFlair(FLARES f) {
        return ((exampleFlairs[activeEffectIndex].flair == f || f == FLARES.none) && ReceiptValidator.GetItem((catalog_items)(f+1)));
    }

    public void SetParticleColor(ParticleSystem[] systems, Color c) {
        foreach (ParticleSystem ps in systems) {
            ps.Stop();
            ps.startColor = c;
            ps.Play();
        }
    }

    public THEMES GetTheme() {
        return validTheme;
    }

    public FLARES GetFlair() {
        return validFlair;
    }

}
