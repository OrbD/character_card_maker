﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Text.RegularExpressions;

public class DataCompiler : MonoBehaviour {

    public GameObject body;
    public GameObject classPicker;
    public GameObject alignmentPicker;
    public GameObject abilityPicker;
    public GameObject otherPicker;
    public GameObject skillPicker;
    public GameObject attackPicker;
    public GameObject appearancePicker;
    public GameObject flairPicker;
    public GameObject colorPicker;
    public GameObject portraitDownloader;


    public InputField nameInput;
    public InputField raceInput;
    public Text classButton;
    public Text alignmentButton;
    public InputField levelInput;
    public InputField initiativeInput;
    public InputField perceptionInput;

    public InputField strengthInput;
    public InputField dexterityInput;
    public InputField consitutionInput;
    public InputField intelligenceInput;
    public InputField wisdomInput;
    public InputField charismaInput;

    public Toggle strSaveToggle;
    public Toggle dexSaveToggle;
    public Toggle conSaveToggle;
    public Toggle intSaveToggle;
    public Toggle wisSaveToggle;
    public Toggle chaSaveToggle;

    public InputField healthInput;
    public InputField armourInput;
    public InputField senseInput;

    public ColorEntry primaryInput;
    public ColorEntry secondaryInput;
    public ColorEntry tertiaryInput;
    public ColorEntry trimInput;
    public ColorEntry trim2Input;
    public ColorEntry highlightInput;
    public ColorEntry textColorInput;
    public PortraitEntry portraitImage;

    public AttackEntry attackOne;
    public AttackEntry attackTwo;
    public AttackEntry attackThree;

    public FlairSetter flairMenu;

    private bool classSet = false;
    private CLASSES storedClass;
    private bool alignmentSet = false;
    private ALIGNMENTS storedAlignment;
    private THEMES storedTheme;
    private FLARES storedFlair;
    private int[] storedSkillProfs = new int[18];
    public Text[] skillProfMarkers;

    void Start() {
        body.SetActive(true) ;
        classPicker.SetActive(false);
        alignmentPicker.SetActive(false);
        abilityPicker.SetActive(false);
        skillPicker.SetActive(false);
        otherPicker.SetActive(false);
        attackPicker.SetActive(false);
        appearancePicker.SetActive(false);
        flairPicker.SetActive(false);
        colorPicker.SetActive(false);
        portraitDownloader.SetActive(false);
    }

	public void SaveClicked() {
        if (CheckFields()) {
            bool newDataAddedFlag = false;
            string newData = CompileData();
            string[] existingData = DataReader.GetJsonArray();
            string buildData = "[";
            foreach(string s in existingData) {
                if (!newDataAddedFlag && Regex.Match(s, "\"time_stamp\": \"(.*)\",").Groups[1].Value + " " + Regex.Match(s, "\"name\": \"(.*)\",").Groups[1].Value == editingCharacterKey) {
                    buildData += newData + ",";
                    newDataAddedFlag = true;
                } else {
                    buildData += s + ",";
                }
            }
            if(!newDataAddedFlag)
                buildData += newData;
            else if(!string.IsNullOrEmpty(buildData))
                buildData = buildData.Remove(buildData.Length-1);
            buildData += "]";

            StreamWriter sw = new StreamWriter(DataReader.BuildPath(DataReader.FOLDER_PATH, DataReader.FILE_NAME));
            sw.Write(buildData);
            sw.Close();

            SceneParser.instance.LoadOpeningMenu();
        }
    }

    private bool CheckFields() {
        if (string.IsNullOrEmpty(nameInput.text)) return false;
        if (string.IsNullOrEmpty(raceInput.text)) return false;
        if (!classSet || !alignmentSet) return false;

        if (string.IsNullOrEmpty(levelInput.text)) levelInput.text = "1";
        if (string.IsNullOrEmpty(strengthInput.text)) strengthInput.text = "10";
        if (string.IsNullOrEmpty(dexterityInput.text)) dexterityInput.text = "10";
        if (string.IsNullOrEmpty(consitutionInput.text)) consitutionInput.text = "10";
        if (string.IsNullOrEmpty(intelligenceInput.text)) intelligenceInput.text = "10";
        if (string.IsNullOrEmpty(wisdomInput.text)) wisdomInput.text = "10";
        if (string.IsNullOrEmpty(charismaInput.text)) charismaInput.text = "10";
        if (string.IsNullOrEmpty(perceptionInput.text))
            perceptionInput.text = (
                10 + StatBlock.CalculateStatMod(int.Parse(wisdomInput.text)) + 
                CharacterData.GetProfBonus(int.Parse(levelInput.text)) / 2.0f * storedSkillProfs[11]
                ).ToString();
        if (string.IsNullOrEmpty(healthInput.text)) {
            int hitDie = CharacterData.GetHitDiceByClass(storedClass);
            int HP = hitDie + StatBlock.CalculateStatMod(int.Parse(consitutionInput.text)) +
                (hitDie / 2 + 1 + StatBlock.CalculateStatMod(int.Parse(consitutionInput.text))) * (int.Parse(levelInput.text) - 1);
            healthInput.text = HP.ToString();
        }
        if (string.IsNullOrEmpty(armourInput.text)) armourInput.text = (10 + StatBlock.CalculateStatMod(int.Parse(dexterityInput.text))).ToString();
        if (string.IsNullOrEmpty(initiativeInput.text)) initiativeInput.text = StatBlock.CalculateStatMod(int.Parse(dexterityInput.text)).ToString();
        if (string.IsNullOrEmpty(senseInput.text)) senseInput.text = "Normal Vision";

        return true;
    }

    private string editingCharacterKey = "";

    public void SetNewData(CharacterData cd, CardColors cc, string portraitPath) {
        editingCharacterKey = cd.timestamp + " " + cd.name;
        nameInput.text = cd.name;
        raceInput.text = cd.race;
        SetClass ((int)cd.playerClass);
        SetAlignment((int)cd.alignment);
        levelInput.text = cd.level.ToString();
        perceptionInput.text = cd.passivePerception.ToString();

        strengthInput.text = cd.stats.Strength.ToString();
        dexterityInput.text = cd.stats.Dexterity.ToString();
        consitutionInput.text = cd.stats.Constitution.ToString();
        intelligenceInput.text = cd.stats.Intelligence.ToString();
        wisdomInput.text = cd.stats.Wisdom.ToString();
        charismaInput.text = cd.stats.Charisma.ToString();

        strSaveToggle.isOn = cd.stats.StrSave;
        dexSaveToggle.isOn = cd.stats.DexSave;
        conSaveToggle.isOn = cd.stats.ConSave;
        intSaveToggle.isOn = cd.stats.IntSave;
        wisSaveToggle.isOn = cd.stats.WisSave;
        chaSaveToggle.isOn = cd.stats.ChaSave;

        healthInput.text = cd.health.ToString();
        armourInput.text = cd.armourClass.ToString();
        initiativeInput.text = cd.initiative.ToString();
        senseInput.text = cd.vision.ToString();

        for(SKILLS i = 0; i <= SKILLS.SURVIVAL; i++) {
            SetSkill((int)i, cd.skillProfs[(int)i]);
        }

        attackOne.SetAttack(cd.attacks[0]);
        attackTwo.SetAttack(cd.attacks[1]);
        attackThree.SetAttack(cd.attacks[2]);

        flairMenu.SetTheme(cd.theme);
        flairMenu.SetFlair(cd.flare);

        primaryInput.SetColor(cc.primary);
        secondaryInput.SetColor(cc.secondary);
        tertiaryInput.SetColor(cc.tertiary);
        trimInput.SetColor(cc.trim);
        trim2Input.SetColor(cc.secondaryTrim);
        highlightInput.SetColor(cc.highlight);
        textColorInput.SetColor(cc.textColor);
        portraitImage.OnConfirm(portraitPath, cd.portraitImage, cd.offsetInfo);
    }

    private string CompileData() {
        CHARACTER_JSON_OBJ parser = new CHARACTER_JSON_OBJ();
        parser.time_stamp = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm");
        parser.name = nameInput.text;
        parser.race = raceInput.text;
        parser.p_class = storedClass.ToString();
        parser.alignment = storedAlignment.ToString();
        parser.level = int.Parse(levelInput.text);
        parser.p_perception = int.Parse(perceptionInput.text);

        parser.str = int.Parse(strengthInput.text);
        parser.dex = int.Parse(dexterityInput.text);
        parser.con = int.Parse(consitutionInput.text);
        parser.inte = int.Parse(intelligenceInput.text);
        parser.wis = int.Parse(wisdomInput.text);
        parser.cha = int.Parse(charismaInput.text);

        parser.str_save = strSaveToggle.isOn;
        parser.dex_save = dexSaveToggle.isOn;
        parser.con_save = conSaveToggle.isOn;
        parser.int_save = intSaveToggle.isOn;
        parser.wis_save = wisSaveToggle.isOn;
        parser.cha_save = chaSaveToggle.isOn;

        parser.init_bonus = (int)float.Parse(initiativeInput.text);
        parser.hp = int.Parse(healthInput.text);
        parser.ac = int.Parse(armourInput.text);
        parser.senses = senseInput.text;

        parser.acro_prof = storedSkillProfs[0];
        parser.anim_prof = storedSkillProfs[1];
        parser.arca_prof = storedSkillProfs[2];
        parser.athl_prof = storedSkillProfs[3];
        parser.dece_prof = storedSkillProfs[4];
        parser.hist_prof = storedSkillProfs[5];
        parser.insi_prof = storedSkillProfs[6];
        parser.inti_prof = storedSkillProfs[7];
        parser.inve_prof = storedSkillProfs[8];
        parser.medi_prof = storedSkillProfs[9];
        parser.natu_prof = storedSkillProfs[10];
        parser.perc_prof = storedSkillProfs[11];
        parser.perf_prof = storedSkillProfs[12];
        parser.pers_prof = storedSkillProfs[13];
        parser.reli_prof = storedSkillProfs[14];
        parser.slei_prof = storedSkillProfs[15];
        parser.stea_prof = storedSkillProfs[16];
        parser.surv_prof = storedSkillProfs[17];

        parser.attack_one = attackOne.GetAttack();
        parser.attack_two = attackTwo.GetAttack();
        parser.attack_three = attackThree.GetAttack();

        if (flairMenu.GetTheme() != THEMES.basic && ReceiptValidator.GetItem(ReceiptValidator.GetRequiredItem(flairMenu.GetTheme()))) {
            parser.theme = ReceiptValidator.EncryptTheme(flairMenu.GetTheme());
        } else {
            parser.theme = ReceiptValidator.EncryptTheme(THEMES.basic);
        }
        if (flairMenu.GetFlair() != FLARES.none && ReceiptValidator.GetItem(ReceiptValidator.GetRequiredItem(flairMenu.GetFlair()))) {
            parser.flair = ReceiptValidator.EncryptEffect(flairMenu.GetFlair());
        } else {
            parser.flair = ReceiptValidator.EncryptEffect(FLARES.none);
        }

        parser.color_primary = "#" + ColorUtility.ToHtmlStringRGBA(primaryInput.storedColor);
        parser.color_secondary = "#" + ColorUtility.ToHtmlStringRGBA(secondaryInput.storedColor);
        parser.color_tertiary = "#" + ColorUtility.ToHtmlStringRGBA(tertiaryInput.storedColor);
        parser.color_trim = "#" + ColorUtility.ToHtmlStringRGBA(trimInput.storedColor);
        parser.color_trim2 = "#" + ColorUtility.ToHtmlStringRGBA(trim2Input.storedColor);
        parser.color_highlight = "#" + ColorUtility.ToHtmlStringRGBA(highlightInput.storedColor);
        parser.color_text = "#" + ColorUtility.ToHtmlStringRGBA(textColorInput.storedColor);

        parser.portrait_path = string.IsNullOrEmpty(portraitImage.localImagePath) ? "null" : portraitImage.localImagePath;
        parser.portrait_is_wide = portraitImage.offsetInfo.is_wide;
        parser.portrait_offset = portraitImage.offsetInfo.offset_percentage;
        parser.portrait_sizemod = Mathf.Max(portraitImage.offsetInfo.size_increase_percentage, 1);

        return JsonUtility.ToJson(parser, true);
    }

    public void SetClass(int c) {
        storedClass = (CLASSES)c;
        classSet = true;
        string className = ((CLASSES)c).ToString();
        classButton.text = className[0] + className.ToLower().Substring(1);
    }

    public void SetAlignment(int a) {
        storedAlignment = (ALIGNMENTS)a;
        alignmentSet = true;
        string[] alignmentNames = ((ALIGNMENTS)a).ToString().Split('_');
        string buttontext = " ";
        foreach(string s in alignmentNames) {
            buttontext += s[0] + s.ToLower().Substring(1) + " ";
        }
        alignmentButton.text = buttontext;
    }

    public void SetSkill(int s) {
        char c = ' ';
        if(storedSkillProfs[s] == 0) {
            c = '-';
            storedSkillProfs[s] = 1;
        } else if(storedSkillProfs[s] == 1) {
            c = '+';
            storedSkillProfs[s] = 2;
        } else if(storedSkillProfs[s] == 2) {
            c = 'X';
            storedSkillProfs[s] = 4;
        } else {
            storedSkillProfs[s] = 0;
        }
        skillProfMarkers[s].text = c.ToString();
    }

    public void SetSkill(int s, int overrideProf) {
        char c = ' ';
        storedSkillProfs[s] = overrideProf;
        if (storedSkillProfs[s] == 1) {
            c = '-';
        } else if (storedSkillProfs[s] == 2) {
            c = '+';
        } else if (storedSkillProfs[s] == 4) {
            c = 'X';
        }
        skillProfMarkers[s].text = c.ToString();
    }

    public void SetFlair(int f) {
        storedFlair = (FLARES)f;
    }
    public void SetTheme(int t) {
        storedTheme = (THEMES)t;
    }
}
