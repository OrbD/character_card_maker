﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorEntry : MonoBehaviour {

    public ColorPicker cp;
    public Image colorPreviewer;
    public Color storedColor { get; private set; }

    void Awake() {
        storedColor = colorPreviewer.color;
    }

    public void OpenPicker() {
        cp.OpenColorWindow(storedColor, SetColor);
    }

    void Callback(Color c) {
        
    }

    public void SetColor(Color c) {
        storedColor = c;
        colorPreviewer.color = c;
    }
}
