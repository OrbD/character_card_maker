﻿using System.Text.RegularExpressions;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeleteWindow : MonoBehaviour {

    public Text windowTitle;

    private CardButton cb;

    void Awake() {
        ClickCancel();
    }

    public void OpenWindow(CardButton btn) {
        cb = btn;
        windowTitle.text = "Delete " + (cb.storedCharacterData.name) + "?";
        gameObject.SetActive(true);
    }

    public void ClickConfirm() {
        DeleteCharacter(cb.storedCharacterData.timestamp + " " + cb.storedCharacterData.name);

        SceneParser.instance.LoadOpeningMenu();
    }

    public void ClickCancel() {
        cb = null;
        gameObject.SetActive(false);
    }

    private void DeleteCharacter(string characterKey) {
        string[] existingData = DataReader.GetJsonArray();
        string buildData = "[";
        foreach (string s in existingData) {
            if (Regex.Match(s, "\"time_stamp\": \"(.*)\",").Groups[1].Value + " " + Regex.Match(s, "\"name\": \"(.*)\",").Groups[1].Value == characterKey) {
                Debug.Log(characterKey + " deleted");
            } else {
                buildData += s + ",";
            }
        }
        if (buildData.Length > 1)
            buildData = buildData.Remove(buildData.Length - 1);
        buildData += "]";

        StreamWriter sw = new StreamWriter(DataReader.BuildPath(DataReader.FOLDER_PATH, DataReader.FILE_NAME));
        sw.Write(buildData);
        sw.Close();

    }
}
