﻿using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttackEntry : MonoBehaviour {

    public GameObject regAttackParent;
    public GameObject dcAttackParent;

    public InputField attackNameInput;

    public Toggle isSave;

    public InputField regAttackInput;
    public InputField regDamageInput;

    public InputField dcValueInput;
    public InputField dcStatInput;
    public InputField dcDamageInput;

    public bool AttackFilled() {
        bool notValid = false;
        notValid = (notValid || string.IsNullOrEmpty(attackNameInput.text));
        if (!isSave.isOn) {
            notValid = (notValid || string.IsNullOrEmpty(regAttackInput.text));
            notValid = (notValid || string.IsNullOrEmpty(regDamageInput.text));
        } else {
            notValid = (notValid || string.IsNullOrEmpty(dcValueInput.text));
            notValid = (notValid || string.IsNullOrEmpty(dcStatInput.text));
            notValid = (notValid || string.IsNullOrEmpty(dcDamageInput.text));
        }
        return !notValid;
    }

	public string CompileAttackString() {
        if (isSave.isOn) {
            return string.Format("{0}: {1} save vs DC{2}. On a failed save deal {3}.", attackNameInput.text, dcStatInput.text, dcValueInput.text, dcDamageInput.text);
        } else {
            return string.Format("{0}: {1}{2} to hit. {3}.", attackNameInput.text, int.Parse(regAttackInput.text) >= 0 ? "+" : "", regAttackInput.text, regDamageInput.text);
        }
    }

    void Awake () {
        isSave.isOn = false;
    }

    /*void Update() {
        Debug.Log(GetAttack());
    }*/

    public string GetAttack () {
        if (AttackFilled()) {
            return (CompileAttackString());
        }
        return "";
    }

    public void SetAttack(string attackString) {
        if (!string.IsNullOrEmpty(attackString)) {
            attackNameInput.text = Regex.Match(attackString, "(.*): ").Groups[1].Value;
            if (attackString.Contains("save vs DC")) {
                isSave.isOn = true;
                dcStatInput.text = Regex.Match(attackString, ": (.*) save vs DC").Groups[1].Value;
                dcValueInput.text = Regex.Match(attackString, "save vs DC(.*).").Groups[1].Value;
                dcDamageInput.text = Regex.Match(attackString, "On a failed save deal (.*)").Groups[1].Value;
                if (!string.IsNullOrEmpty(dcDamageInput.text))
                    dcDamageInput.text = dcDamageInput.text.Remove(dcDamageInput.text.Length - 1);
            } else {
                isSave.isOn = false;
                regAttackInput.text = Regex.Match(attackString, ": (.*) to hit").Groups[1].Value;
                regDamageInput.text = Regex.Match(attackString, "to hit. (.*)").Groups[1].Value;
                if (!string.IsNullOrEmpty(regDamageInput.text))
                    regDamageInput.text = regDamageInput.text.Remove(regDamageInput.text.Length - 1);
            }
        }
    }

    public void OnToggleChange(bool isOn) {
        if (isOn) {
            dcAttackParent.SetActive(true);
            regAttackParent.SetActive(false);
        } else {
            dcAttackParent.SetActive(false);
            regAttackParent.SetActive(true);
        }
    }
}
