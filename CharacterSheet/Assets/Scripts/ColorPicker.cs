﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorPicker : MonoBehaviour {

    public delegate void D_COLOR_CALLBACK(Color returnColor);

    public RectTransform contentWindow;
    public ColorColumn exampleColumn;
    public Image colorDisplay;
    public InputField hexInput;
    public Slider saturationSlider;
    public Text saturationText;

    private Color startColor;
    private Color storedColor = Color.white;
    private float storedSaturation = 1;
    private D_COLOR_CALLBACK storedCallback;

    void Awake () {
        exampleColumn.Init(this);

        for (int row = 0; row < exampleColumn.buttons.Length; row++) {
            exampleColumn.buttons[row].color = Color.HSVToRGB(0, 0, 1 - row / (exampleColumn.buttons.Length + 0.0f));
        }

        for (int column = 0; column < 24; column++) {
            ColorColumn temp = Instantiate<GameObject>(exampleColumn.gameObject, contentWindow).GetComponent<ColorColumn>();
            for (int row = 0; row < exampleColumn.buttons.Length; row++) {
                temp.buttons[row].color = Color.HSVToRGB(column / 24.0f, 1, 1-row / (temp.buttons.Length + 0.0f));
            }
        }
        contentWindow.sizeDelta = new Vector2(1705, contentWindow.sizeDelta.y);

        ClickOverride();
    }

    public void OpenColorWindow(Color currentColor, D_COLOR_CALLBACK callback) {
        gameObject.SetActive(true);
        storedCallback = callback;
        startColor = currentColor;
        hexInput.text = ColorUtility.ToHtmlStringRGB(currentColor);
        ClickOverride();
    }

    public void SaveColor() {
        Color c;
        GetColor(out c);
        storedCallback(c);
        gameObject.SetActive(false);
    }

    public void Cancel() {
        storedCallback(startColor);
        gameObject.SetActive(false);
    }

    public void RecieveColor(Color c) {
        storedColor = c;
        float h, s, v = 0;
        Color.RGBToHSV(storedColor, out h, out s, out v);
        if (s != storedSaturation) {
            saturationSlider.value = s;
        } else {
            Color disColor;
            hexInput.text = GetColor(out disColor);
            colorDisplay.color = disColor;
        }
    }

    public void RecieveSaturation(float s) {
        storedSaturation = s;
        Color disColor;
        hexInput.text = GetColor(out disColor);
        saturationText.text = "Saturation " + storedSaturation.ToString("0.00");
        colorDisplay.color = disColor;
    }

    public void ClickOverride() {
        if (hexInput.text.Length >= 6) {
            Color c;
            if (ColorUtility.TryParseHtmlString("#" + hexInput.text, out c)) {
                float h, s, v = 0;
                Color.RGBToHSV(c, out h, out s, out v);

                storedColor = Color.HSVToRGB(h, 1, v);
                storedSaturation = s;
                colorDisplay.color = c;

                saturationSlider.value = s;
            }
        }
    }

    public string GetColor(out Color color) {
        float h, s, v = 0;
        Color.RGBToHSV(storedColor, out h, out s, out v);
        color = Color.HSVToRGB(h, storedSaturation, v);
        return (ColorUtility.ToHtmlStringRGB(color));
    }
}
