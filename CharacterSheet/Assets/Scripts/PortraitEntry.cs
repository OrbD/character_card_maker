﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PortraitEntry : MonoBehaviour {

    public PortraitDownloader pd;
    public string localImagePath;
    public Image displayImage;
    public IMAGE_OFFSET_INFO offsetInfo = new IMAGE_OFFSET_INFO();

	public void Click() {
        pd.OpenURLWindow(OnConfirm);
    }

    public void OnConfirm (string path, Sprite image, IMAGE_OFFSET_INFO _offsetInfo) {
        displayImage.rectTransform.anchoredPosition = Vector2.zero;
        displayImage.rectTransform.sizeDelta = new Vector2(Mathf.Min(displayImage.rectTransform.sizeDelta.x, displayImage.rectTransform.sizeDelta.y), Mathf.Min(displayImage.rectTransform.sizeDelta.x, displayImage.rectTransform.sizeDelta.y));

        displayImage.sprite = image;
        localImagePath = path;
        this.offsetInfo = _offsetInfo;

        if (offsetInfo.is_wide) {
            displayImage.rectTransform.anchoredPosition = new Vector2(displayImage.rectTransform.sizeDelta.x * offsetInfo.offset_percentage, displayImage.rectTransform.anchoredPosition.y);
            displayImage.rectTransform.sizeDelta = new Vector2(displayImage.rectTransform.sizeDelta.x * offsetInfo.size_increase_percentage, displayImage.rectTransform.sizeDelta.y);
        } else {
            displayImage.rectTransform.anchoredPosition = new Vector2(displayImage.rectTransform.anchoredPosition.x, displayImage.rectTransform.sizeDelta.y * offsetInfo.offset_percentage);
            displayImage.rectTransform.sizeDelta = new Vector2(displayImage.rectTransform.sizeDelta.x, displayImage.rectTransform.sizeDelta.y * offsetInfo.size_increase_percentage);
        }
    }
}
