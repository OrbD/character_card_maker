﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneParser : MonoBehaviour {

    public const string CARD_DISPLAY_SCENE_NAME = "CharacterCard";
    public const string OPENING_SCENE_NAME = "OpeningMenu";
    public const string CREATE_CARD_SCENE_NAME = "CreateCard";

    public static SceneParser instance { get; private set; }
    private CharacterData storedData;
    private CardColors storedColors;
    private string imagePath;


	void Start () {
        if (instance == null) {
            instance = this;
            SceneManager.sceneLoaded += OnSceneLoaded;
            DontDestroyOnLoad(this.gameObject);
        } else
            Destroy(this.gameObject);
    }

    public void LoadOpeningMenu() {
        SceneManager.LoadScene(OPENING_SCENE_NAME, LoadSceneMode.Single);
    }

    public void LoadCardMaker(CardButton cb = null) {
        if (cb != null) {
            storedColors = cb.storedCardColors;
            storedData = cb.storedCharacterData;
            imagePath = cb.portraitPath;
        } else {
            storedColors = null;
            storedData = null;
            imagePath = "";
        }
        SceneManager.LoadScene(CREATE_CARD_SCENE_NAME, LoadSceneMode.Single);
    }

    public void LoadCardViewer(CardButton cb) {
        storedColors = cb.storedCardColors;
        storedData = cb.storedCharacterData;
        SceneManager.LoadScene(CARD_DISPLAY_SCENE_NAME, LoadSceneMode.Single);
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        switch (scene.name) {
            case CARD_DISPLAY_SCENE_NAME:
                LoadIntoCardViewer(scene);
                break;
            case OPENING_SCENE_NAME:

                break;
            case CREATE_CARD_SCENE_NAME:
                LoadIntoCardMaker(scene);
                break;
        }
    }

    void LoadIntoCardMaker(Scene s) {
        if (storedData != null && storedColors != null) {
            foreach (GameObject g in s.GetRootGameObjects()) {
                if (g.GetComponent<DataCompiler>()) {
                    g.GetComponent<DataCompiler>().SetNewData(storedData, storedColors, imagePath);
                }
            }
        }
    }

    void LoadIntoCardViewer(Scene s) {
        foreach(GameObject g in s.GetRootGameObjects()) {
            if (g.GetComponent<CardManager>()) {
                g.GetComponent<CardManager>().SetNewData(storedData, storedColors);
            }
        }
    }
}
