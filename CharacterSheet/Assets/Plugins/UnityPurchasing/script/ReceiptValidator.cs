﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public enum catalog_items {
    all_items = -1,
    none,
    dwarven,
    elven,
    pirate,
    tribal,
    infernal,
    celestial,
    arcane
}

public enum FLARES {
    none = -1,
    sun_shafts,
    magic_weave,
    waves,
    flames,
    devils_whispers,
    divine_rain,
    stars
}

public enum THEMES {
    basic,
    dwarven,
    elven,
    pirate,
    tribal,
    fiendish,
    celestial,
    arcane
}

public class ReceiptValidator : MonoBehaviour {

    private static string USER_ID {
        get {
            if (string.IsNullOrEmpty(realUserID))
                InitUserID();
            return realUserID;
        }
    }
    private static string realUserID;
    private static Dictionary<catalog_items, bool> validItems = null;

    void Start() {
        
    }

    public static bool GetItem(catalog_items item) {
        if (validItems == null || !validItems.ContainsKey(item))
            InitCatalogList();
        return validItems[catalog_items.all_items] || validItems[item];
    }

    private static void InitUserID() {
        if (!PlayerPrefs.HasKey("UserID")) {
            PlayerPrefs.SetString("UserID", System.Guid.NewGuid().ToString());
        }
        realUserID = PlayerPrefs.GetString("UserID");
    }

    private static void InitCatalogList() {
        validItems = new Dictionary<catalog_items, bool>();
        for (catalog_items c = catalog_items.all_items; c <= catalog_items.arcane; c++) {
            if (c != catalog_items.none) {
                if (PlayerPrefs.HasKey(c + "_pack_receipt")) {
                    string encryptedKey = GetHashString(USER_ID + "_" + c.ToString());

                    if (PlayerPrefs.GetString(c + "_pack_receipt") == encryptedKey) {
                        validItems.Add(c, true);
                    } else {
                        PlayerPrefs.DeleteKey(c + "_pack_receipt");
                    }
                } else {
                    validItems.Add(c, false);
                }
            } else {
                validItems.Add(c, true);
            }
        }
    }

    public static void AddItem(UnityEngine.Purchasing.Product purchasedProduct) {
        if(validItems == null) {
            InitCatalogList();
        }

        catalog_items item;
        string pID = purchasedProduct.definition.id;
        if (pID.StartsWith("flair_pack.all")) {
            pID = "flair_pack.all";
        }
        switch (pID) {
            case "flair_pack.all":
                item = catalog_items.all_items;
                break;
            case "flair_pack.dwarven":
                item = catalog_items.dwarven;
                break;
            case "flair_pack.elven":
                item = catalog_items.elven;
                break;
            case "flair_pack.arcane":
                item = catalog_items.arcane;
                break;
            case "flair_pack.pirate":
                item = catalog_items.pirate;
                break;
            case "flair_pack.tribal":
                item = catalog_items.tribal;
                break;
            case "flair_pack.infernal":
                item = catalog_items.infernal;
                break;
            case "flair_pack.celestial":
                item = catalog_items.celestial;
                break;
            default:
                return;
        }

        string encryptedKey = GetHashString(USER_ID + "_" + item.ToString());
        Debug.Log(encryptedKey);
        PlayerPrefs.SetString(item + "_pack_receipt", encryptedKey);
        validItems[item] = true;
    }

    public static catalog_items GetRequiredItem(THEMES t) {
        return (catalog_items)(int)t;
    }

    public static catalog_items GetRequiredItem(FLARES f) {
        return (catalog_items)((int)f + 1);
    }

    public static int GetTotalPurchases() {
        int totalPurchases = 0;
        for (catalog_items c = catalog_items.dwarven; c <= catalog_items.arcane; c++) {
            if (validItems.ContainsKey(c) && validItems[c]) {
                totalPurchases++;
            }
        }
        return totalPurchases;
    }

    public static string EncryptTheme(THEMES t) {
        return GetHashString(USER_ID + "_" + t.ToString());
    }
    public static string EncryptEffect(FLARES t) {
        return GetHashString(USER_ID + "_" + t.ToString());
    }

    public static THEMES DecryptTheme(string s) {
        for (THEMES i = THEMES.basic; i <= THEMES.arcane; i++) {
            if (GetHashString(USER_ID + "_" + i.ToString()) == s)
                return i;
        }
        return THEMES.basic;
    }
    public static FLARES DecryptEffect(string s) {
        for (FLARES i = FLARES.none; i <= FLARES.stars; i++) {
            if (GetHashString(USER_ID + "_" + i.ToString()) == s)
                return i;
        }
        return FLARES.none;
    }

    private static string GetHashString(string toHash) {
        RIPEMD160 rip = new RIPEMD160Managed();
        byte[] toBytes = System.Text.Encoding.UTF8.GetBytes(toHash);
        byte[] hashedBytes = rip.ComputeHash(toBytes);
        return ConvertHashToString(hashedBytes);
    }

    private static string ConvertHashToString(byte[] array) {
        string s = "";
        int i;
        for (i = 0; i < array.Length; i++) {
            s += (string.Format("{0:X2}", array[i]));
            //if ((i % 4) == 3) s += (" ");
        }
        return s.ToLower();
    }
}
