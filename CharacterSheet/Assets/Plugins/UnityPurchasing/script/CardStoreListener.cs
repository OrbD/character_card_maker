﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class CardStoreListener : IStoreListener {

    // IStoreListener

    IStoreController store;
    IExtensionProvider provider;

    public static bool IsInitialised = false;

    public static System.Action<bool> StoreInitialised;

    public void OnInitializeFailed(InitializationFailureReason error) {
        Debug.Log("Init Failed: " + error.ToString());
        StoreInitialised(false);
        IsInitialised = false;
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e) {
        ReceiptValidator.AddItem(e.purchasedProduct);
        return PurchaseProcessingResult.Complete;
    }

    public void OnPurchaseFailed(Product i, PurchaseFailureReason p) {
        Debug.Log("Purchase Failed: " + p.ToString());
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions) {
        store = controller;
        provider = extensions;
        if(StoreInitialised != null) {
            StoreInitialised(true);
        }
        IsInitialised = true;
    }
}

